/*
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License. See LICENSE in the project root for
 * license information.
 */

package com.microsoft.azure.samples.spring.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microsoft.azure.samples.spring.model.Pet;
import com.microsoft.azure.samples.spring.repository.PetRepository;
import com.microsoft.azure.samples.spring.service.AzureBlobAdapter;
import com.microsoft.azure.samples.spring.service.AzureQueueAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.util.Optional;


@Controller
@RequestMapping(path = "/pets")
public class PetController {
    @Autowired
    private PetRepository petRepository;

    @Autowired
    private AzureBlobAdapter azureBlobAdapter;

    @Autowired
    private AzureQueueAdapter azureQueueAdapter;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping
    public ResponseEntity upload(@RequestParam("pet") String pet, @RequestParam("file") MultipartFile multipartFile) throws IOException {
        URI url = azureBlobAdapter.upload(multipartFile);
        Pet petJson = objectMapper.readValue(pet, Pet.class);
        petJson.setFileUrl(url.toString());
        petRepository.save(petJson);
        return ResponseEntity.ok(url);
    }

   /* @PostMapping
    public @ResponseBody String createPet(@RequestBody Pet pet) {
        petRepository.save(pet);
        return String.format("Added %s.", pet);
    }*/

    @GetMapping
    public @ResponseBody
    Iterable<Pet> getAllPets() {
        return petRepository.findAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody
    Optional<Pet> getPet(@PathVariable Integer id) {
        return petRepository.findById(id);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody
    String deletePet(@PathVariable Integer id) {
        petRepository.deleteById(id);
        return "Deleted " + id;
    }
}
