package com.microsoft.azure.samples.spring.service;

import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import com.microsoft.azure.storage.queue.CloudQueue;
import com.microsoft.azure.storage.queue.CloudQueueClient;
import com.microsoft.azure.storage.queue.CloudQueueMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Component
public class AzureQueueAdapter {

    private static final Logger logger = LoggerFactory.getLogger(AzureQueueAdapter.class);

    @Autowired
    private CloudQueueClient cloudQueueClient;

    @Autowired
    private CloudQueue cloudQueue;

    public URI upload(MultipartFile multipartFile){
        URI uri = null;
        CloudQueue queue = null;
        try {
            queue = cloudQueueClient.getQueueReference("myqueue");
            queue.createIfNotExists();
            InputStream inputStream = multipartFile.getInputStream();
            byte[] targetArray = new byte[inputStream.available()];
            CloudQueueMessage message = new CloudQueueMessage(targetArray);
            queue.addMessage(message);
            uri = queue.getUri();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (StorageException e) {
            e.printStackTrace();
        }catch (IOException e) {
            e.printStackTrace();
        }
        return uri;
    }
}
